terraform {
  required_version = ">= 0.12"

  backend "s3" {
    endpoint                    = "https://ams3.digitaloceanspaces.com"
    bucket                      = "terraform-state-2"
    key                         = "global/s3/terraform.tfstate"
    region                      = "eu-west-1"
    skip_requesting_account_id  = true
    skip_credentials_validation = true
    skip_get_ec2_platforms      = true
    skip_metadata_api_check     = true
  }
}

# ------------------------------------------------------------------------------
# CONFIGURE OUR DO CONNECTION
# ------------------------------------------------------------------------------

provider "digitalocean" {
  token = "${var.do_token}"

  spaces_access_id  = "${var.spaces_access_id}"
  spaces_secret_key = "${var.spaces_secret_key}"
}

# ------------------------------------------------------------------------------
# CREATE THE S3 BUCKET
# ------------------------------------------------------------------------------

resource "digitalocean_spaces_bucket" "terraform_state" {
  name = "terraform-state-2"
  region = "ams3"
  acl = "private"
}
