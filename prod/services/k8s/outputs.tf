output "cluster-id" {
  value = "${digitalocean_kubernetes_cluster.remote_execution_cluster.id}"
}
