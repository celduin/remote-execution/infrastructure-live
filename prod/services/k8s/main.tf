terraform {
  required_version = ">= 0.12"

  backend "s3" {
    endpoint                    = "https://ams3.digitaloceanspaces.com"
    bucket                      = "terraform-state-2"
    key                         = "prod/services/k8s/terraform.tfstate"
    region                      = "eu-west-1"
    skip_requesting_account_id  = true
    skip_credentials_validation = true
    skip_get_ec2_platforms      = true
    skip_metadata_api_check     = true
  }
}

provider "digitalocean" {
  token = var.do_token
}

resource "digitalocean_kubernetes_cluster" "remote_execution_cluster" {
  name    = "remote-execution-cluster-prod"
  region  = "ams3"
  version = "1.15.5-do.1"

  node_pool {
    name       = "worker-pool"
    size       = "s-2vcpu-2gb"
    node_count = 2
  }
}
